/*
    erdem çalışır
*/
(function () {

    var KEY = {
        UP: 38,
        DOWN: 40,
        W: 87,
        S: 83
    };
    
    var CONSTS = {
    	gameSpeed: 20,
        score1: 0,
        score2: 0,
        stick1Speed: 0,
        stick2Speed: 0,
        ballTopSpeed: 0,
        ballLeftSpeed: 0,
        canvasHeight: 600,
        canvasWitdh: 900
    };

    var CSS = {
        arena: {
            width: 900,
            height: 600,
            background: '#62247B',
            position: 'fixed',
            top: '50%',
            left: '50%',
            zIndex: '999',
            transform: 'translate(-50%, -50%)'
        },
        ball: {
            width: 15,
            height: 15,
            position: 'absolute',
            top: 0,
            left: 350,
            borderRadius: 50,
            background: '#C6A62F'
        },
        line: {
            width: 0,
            height: 600,
            borderLeft: '2px dashed #C6A62F',
            position: 'absolute',
            top: 0,
            left: '50%'
        },
        stick: {
            width: 12,
            height: 85,
            position: 'absolute',
            background: '#C6A62F'
        },
        stick1: {
            left: 0,
            top: 0
        },
        stick2: {
            left: CONSTS.canvasWitdh -12,
            top: 0
        },
        centerCircle:{
            top: CONSTS.canvasHeight/2-100,
            width: 200,
            height: 200,
            position: 'relative',
            left: CONSTS.canvasWitdh/2-100 ,
            borderRadius: 100,
            border: '2px dashed #C6A62F'
        },
        scoreText1:{
            top:0,
            left: 0
        },
        scoreText2:{
            top:0,
            left: 0
        }
    };

    function start() {
        draw();
        $( "score-left" ).html( "<div class='red'>Hello <b>Again</b></div>" )
        setEvents();
        roll();
        loop();
        
    }

    function draw() {
        $('<div/>', {id: 'pong-game'}).css(CSS.arena).appendTo('body');
        $('<div/>', {id: 'pong-line'}).css(CSS.line).appendTo('#pong-game');
        $('<div/>', {id: 'pong-ball'}).css(CSS.ball).appendTo('#pong-game');
        $('<div/>', {id: 'pong-center'}).css(CSS.centerCircle).appendTo('#pong-game');
        $('<div/>', {id: 'stick-1'}).css($.extend(CSS.stick1, CSS.stick)).appendTo('#pong-game');
        $('<div/>', {id: 'stick-2'}).css($.extend(CSS.stick2, CSS.stick)).appendTo('#pong-game');
        $('<div/>', {id: 'score-left'}).css(CSS.scoreText1).appendTo('body');
        $('<div/>', {id: 'score-right'}).css(CSS.scoreText2).appendTo('body');
        $('<div/>', {id: 'scoreboard'}).css(CSS.scoreText2).appendTo('body');
    }
   
    


    function setEvents() {
        $(document).on('keydown', function (e) {
            if (e.keyCode == KEY.S) {
                CSS.stick1.top = CSS.stick1.top +50;
                if(CSS.stick1.top+CSS.stick1.height > CSS.arena.height)
                {
                    CSS.stick1.top =CSS.arena.height-CSS.stick1.height;
                }
            }
        });

        $(document).on('keyup', function (e) {
            if (e.keyCode == KEY.W) {
                CSS.stick1.top = CSS.stick1.top -50;
                if(CSS.stick1.top < 0)
                {
                    CSS.stick1.top =0;
                }
            }
        });
        $(document).on('keydown', function (e) {
            if (e.keyCode == KEY.DOWN) {
                CSS.stick2.top = CSS.stick2.top +50;
                if(CSS.stick2.top+CSS.stick2.height > CSS.arena.height)
                {
                    CSS.stick2.top =CSS.arena.height-CSS.stick2.height;
                }
            }
        });
        $(document).on('keyup', function (e) {
            if (e.keyCode == KEY.UP) {
                CSS.stick2.top = CSS.stick2.top -50;
                if(CSS.stick2.top < 0)
                {
                    CSS.stick2.top =0;
                }
            }
        });
    }

    function loop() {
        window.pongLoop = setInterval(function () {

            CSS.stick1.top += CONSTS.stick1Speed;
            // CSS.stick2.top += CONSTS.stick2Speed;
            

            $('#stick-1').css('top', CSS.stick1.top);
            $('#stick-2').css('top', CSS.stick2.top);
            
            if(CSS.stick1.top > CONSTS.canvasHeight- CSS.stick1.height ||  CSS.stick1.top <= 0)
            {
                CONSTS.stick1Speed = -CONSTS.stick1Speed;
                
            }
            if(CSS.stick2.top > CONSTS.canvasHeight- CSS.stick2.height ||  CSS.stick2.top <= 0)
            {
                CONSTS.stick2Speed = -CONSTS.stick2Speed;
                
            }
    

            CSS.ball.top += CONSTS.ballTopSpeed;
            CSS.ball.left += CONSTS.ballLeftSpeed;
           

            if (CSS.ball.top <= 0 || CSS.ball.top >= CSS.arena.height - CSS.ball.height) {
                CONSTS.ballTopSpeed = CONSTS.ballTopSpeed * -1;
                //CONSTS.ballLeftSpeed = CONSTS.ballLeftSpeed * -1;
                
                
            }

            $('#pong-ball').css({top: CSS.ball.top,left: CSS.ball.left});

            
            if(CSS.stick1.top <= CSS.ball.top && CSS.ball.top <= CSS.stick1.top+CSS.stick1.height )
            {
                if(CSS.ball.left < 12 )
                {
                    CONSTS.ballLeftSpeed = -CONSTS.ballLeftSpeed;
                    console.log("çarpışma sol");  
                }   
            }
            else if(CSS.ball.left <= CSS.stick.width){

                CONSTS.score2++;
                console.log("gool user 2 :score " + CONSTS.score2);
                if(CONSTS.score2 == 2)
                {
                    winner2();
                }
                //alert("GOOL USER 2");
                roll();
            }

            
            if((CSS.stick2.top <= CSS.ball.top && CSS.ball.top <= CSS.stick2.top+CSS.stick2.height) )
            {
                if(CSS.ball.left > 871 )
                {
                    CONSTS.ballLeftSpeed = -CONSTS.ballLeftSpeed;
                    console.log("çarpışma SAĞ");
                }
            }
            else if(CSS.ball.left >= CSS.arena.width - CSS.ball.width - CSS.stick.width){
                CONSTS.score1++;
                console.log("gool user 1 :score " + CONSTS.score1); 
                //alert("GOOL USER 1");
                if(CONSTS.score1 == 2)
                {
                    winner1();
                }
                roll();
            }

        }, CONSTS.gameSpeed);

        
    }

    function roll() {
        CSS.ball.top = CSS.arena.height/2-7.5;
        CSS.ball.left = CSS.arena.width/2-7.5;
        
        // CONSTS.ballTopSpeed =3;
        // CONSTS.ballLeftSpeed =4;
        // CSS.ball.top = 250;
        // CSS.ball.left = 350;

        var side = -1;

        if (Math.random() < 0.5) {
            side = 1;
        }

        CONSTS.ballTopSpeed = Math.random() * -2 - 3;
        CONSTS.ballLeftSpeed = side * (Math.random() * 2 + 3);
    }
    function winner2()
    {
        console.log("winner user 2");
        CONSTS.ballTopSpeed =0;
        CONSTS.ballLeftSpeed =0;
        CSS.ball.top = 0;
        CSS.ball.left = 0;
        return false;
    }
    function winner1()
    {
        console.log("winner user 1");
        CONSTS.ballTopSpeed =0;
        CONSTS.ballLeftSpeed =0;
        CSS.ball.top = 0;
        CSS.ball.left = 0;
        CSS.ball.width=0;
        CSS.ball.height=0;
        return false;
    }

    start();

    
})();

